package ir.phsys.sitereader.util


import org.slf4j.LoggerFactory


/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
object StopWatch {
  val logger=LoggerFactory.getLogger(this.getClass.getName)
  def apply[A](x: => A): A ={
    val start=System.currentTimeMillis()
    val result=x
    val end=System.currentTimeMillis()-start
    logger.debug(s"Operation finished in $end")
    result
  }
  def apply[A](message:String)(x: => A): A ={
    val start=System.currentTimeMillis()
    val result=x
    val end=System.currentTimeMillis()-start
    logger.debug(s"$message finished in $end")
    result
  }
}
