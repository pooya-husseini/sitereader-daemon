package ir.phsys.sitereader.solr;

import ir.phsys.sitereader.solr.annotations.Highlightable;
import ir.phsys.sitereader.solr.annotations.Increment;
import ir.phsys.sitereader.util.DateUtils;
import org.apache.solr.client.solrj.beans.Field;
import org.apache.solr.common.SolrInputDocument;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
public class ContentBean {
    private static final String DEFAULT = "#default";
    private static List<PropertyDescriptor> propertyDescriptors = new ArrayList<>();
    private static final Map<String, String> FIELD_NAME_MAP = new HashMap<>();

    static {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(ContentBean.class);
            PropertyDescriptor[] propertyDescriptorsArr = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorsArr) {
                if (!propertyDescriptor.getName().equals("class")) {
                    propertyDescriptors.add(propertyDescriptor);
                }
            }
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    private String id;
    private String title;
    private String text;
    private String content;
    private String subject;
    private String description;
    private String comment;
    private String keywords;
    private String category;
    private String links;
    private String publisher;
    private String url;
    private Date pubDate;
    private Boolean archived;
    private String status;
    private Integer updateCount;

    public ContentBean() {
    }

    public ContentBean(String id, String title, String text, String content, String links, String publisher, String url, Date pubDate, Boolean archived, String status) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.links = links;
        this.publisher = publisher;
        this.url = url;
        this.pubDate = pubDate;
        this.archived = archived;
        this.status = status;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    @Field("title_t")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    //    @Field
    public void setId(String id) {
        this.id = id;
    }

    @Highlightable
    public String getText() {
        return text;
    }

    @Field("persian_text")
    public void setText(String text) {
        this.text = text;
    }

    public String getContent() {
        return content;
    }

    @Field("persian_content")
    public void setContent(String content) {
        this.content = content;
    }

    public String getPublisher() {
        return publisher;
    }

    @Field("author")
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getUrl() {
        return url;
    }

    @Field("url_t")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    //    @Field
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    //    @Field
    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    //    @Field
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getKeywords() {
        return keywords;
    }

    //    @Field
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCategory() {
        return category;
    }

    //    @Field
    public void setCategory(String category) {
        this.category = category;
    }

    public String getLinks() {
        return links;
    }

    @Field("links_t")
    public void setLinks(String links) {
        this.links = links;
    }

    public Date getPubDate() {
        return pubDate;
    }

    @Field("pubDate_dt")
    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }


    public Boolean getArchived() {
        return archived;
    }

    @Field("archived_b")
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public String getStatus() {
        return status;
    }

    @Field("status_s")
    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, String> toMap(Boolean addStar) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        return toMap(addStar, 0);
    }

    public Integer getUpdateCount() {
        return updateCount;
    }

    @Increment
    @Field("updateCount")
    public void setUpdateCount(Integer updateCount) {
        this.updateCount = updateCount;
    }

    public static Set<String> highlightableField() {
        Set<String> stringSet = new HashSet<>();

        for (PropertyDescriptor descriptor : propertyDescriptors) {
            if (descriptor.getReadMethod().isAnnotationPresent(Highlightable.class) ||
                    descriptor.getWriteMethod().isAnnotationPresent(Highlightable.class)) {
                stringSet.add(retrieveFieldName(descriptor));
            }
        }

        return stringSet;
    }


    private static String retrieveFieldName(PropertyDescriptor descriptor) {
        String value = FIELD_NAME_MAP.get(descriptor.getName());

        if (value != null) {
            return value;
        }
        if (descriptor.getWriteMethod().isAnnotationPresent(Field.class)) {
            Field annotation = descriptor.getWriteMethod().getAnnotation(Field.class);
            value = annotation.value();
        } else {
            value = descriptor.getName();
        }
        FIELD_NAME_MAP.put(descriptor.getName(), value);
        return value;
    }

    public Map<String, String> toMap(Boolean addStar, int daysEarlier) throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        Map<String, String> stringMap = new HashMap<>();

        for (PropertyDescriptor pd : propertyDescriptors) {
            Object invoke = pd.getReadMethod().invoke(this);
            if (invoke != null) {
                if (pd.getWriteMethod().isAnnotationPresent(Field.class)) {
                    Field annotation = pd.getWriteMethod().getAnnotation(Field.class);
                    String value = "";
                    if (pd.getWriteMethod().isAnnotationPresent(Increment.class)) {
// to do the code
                    } else if (invoke.getClass().isAssignableFrom(Date.class)) {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                        Date startOfDay = DateUtils.startOfDay((Date) invoke, daysEarlier);

                        Date endOfDay = DateUtils.endOfDay((Date) invoke);


                        value = "[" + dateFormat.format(startOfDay) + " TO " + dateFormat.format(endOfDay) + "]";

                    } else {
                        if (addStar) {
                            value = "*" + invoke.toString() + "*";
                        } else {
                            value = invoke.toString();
                        }
                    }

                    if (annotation.value().equals(DEFAULT)) {
                        stringMap.put(pd.getName(), value);
                    } else {
                        stringMap.put(annotation.value(), value);
                    }
                } else {
                    stringMap.put(pd.getName(), invoke.toString());
                }
            }
        }
        return stringMap;
    }

    public void fillByMap(Map<String, String> objectMap) throws InvocationTargetException, IllegalAccessException {
        for (PropertyDescriptor descriptor : propertyDescriptors) {
            Method writeMethod = descriptor.getWriteMethod();
            String propName = descriptor.getName();
            if (writeMethod.isAnnotationPresent(Field.class)) {
                propName = writeMethod.getAnnotation(Field.class).value();
            }

            if (objectMap.containsKey(propName)) {
                writeMethod.invoke(this, objectMap.get(propName));
            }
        }
    }

    public void changeFieldSize(Map<String, Integer> fieldSizeMap) throws InvocationTargetException, IllegalAccessException {
        for (PropertyDescriptor descriptor : propertyDescriptors) {
            Method writeMethod = descriptor.getWriteMethod();
            String propName = descriptor.getName();
            if (writeMethod.isAnnotationPresent(Field.class)) {
                propName = writeMethod.getAnnotation(Field.class).value();
            }

            if (fieldSizeMap.containsKey(propName)) {
                String value = String.valueOf(descriptor.getReadMethod().invoke(this));
                writeMethod.invoke(this, value.substring(0, fieldSizeMap.get(propName)));
            }
        }
    }

    public SolrInputDocument toSolrDocument() throws InvocationTargetException, IllegalAccessException {
        SolrInputDocument document = new SolrInputDocument();
        for (PropertyDescriptor descriptor : propertyDescriptors) {
            Object invoke = descriptor.getReadMethod().invoke(this);
            if (descriptor.getWriteMethod().isAnnotationPresent(Increment.class)) {
                Map<String, Object> map = new HashMap<>();
                map.put("inc", 1);
                document.addField(retrieveFieldName(descriptor), map);

            } else if (invoke != null) {
                document.addField(retrieveFieldName(descriptor), invoke);
            }
        }
        return document;
    }
}
